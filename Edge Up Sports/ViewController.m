//
//  ViewController.m
//  Edge Up Sports
//
//  Created by Vitalii Budnik on 6/7/16.
//  Copyright © 2016 Vitalii Budnik. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end
