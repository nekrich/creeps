//
//  AppDelegate.h
//  Edge Up Sports
//
//  Created by Vitalii Budnik on 6/7/16.
//  Copyright © 2016 Vitalii Budnik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

